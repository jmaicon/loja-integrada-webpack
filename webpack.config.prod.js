const path = require('path')
const webpack = require('webpack')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const pkg = require('./package')

module.exports = {
  mode: 'production',
  entry: [
    './src/js/index.js',
    './src/scss/index.scss'
  ],
  devServer: {
    contentBase: './dist',
    hot: false
  },
  module: {
    rules: [
      { 
        test: /\.m?js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          {
            loader: 'sass-loader',
          },
          'postcss-loader'
        ]
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new MiniCssExtractPlugin({
      filename: `${pkg.accountName}.min.css`
    }),
  ],
  output: {
    filename: `${pkg.accountName}.min.js`,
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/'
  }
}
